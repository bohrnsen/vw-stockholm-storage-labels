<?php require_once 'PHPExcel_1.8.0/Classes/PHPExcel.php';

$myFile = "Preisauszeichnung/".date("Y-m-d_Hi").".paz";
$fh = fopen($myFile, 'w') or die("can't open file");
$stringData = "V4.0|LAG|LA2\n";
fwrite($fh, $stringData);
foreach ($_FILES["pazs"]["tmp_name"] as $name) {
	$inputFileName = $name;
	$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
	$objReader = PHPExcel_IOFactory::createReader($inputFileType);
	$objReader->setReadDataOnly(true);
	$objPHPExcel = $objReader->load($inputFileName);
	$objWorksheet = $objPHPExcel->getActiveSheet();
	
	//fwrite($fh, $stringData);
	foreach ($objWorksheet->getRowIterator() as $row) {
 		$cellIterator = $row->getCellIterator();
  		$cells = array();
		foreach ($cellIterator as $cell) {
			$value = $cell->getValue();
			if (strlen($value) > 0){
				$cells[] = $value;
			}
		}
		$stringData = '1|'.$cells[0].'|$0||$0|0|$0||10|$0|'.utf8_decode($cells[1]).'|||0|$0||0|$0|||||$0|'.$cells[2].'|0|$0||||$0|0|0|1|$0|0|0|$0|0||1|0'."\n";
		fwrite($fh, $stringData);
	}
}
fclose($fh);
echo "upload/".$myFile;
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>Etiketter</title>
</head>
<body>
	<form id="file-form" action="upload/uploader.php" method="POST">
		<input type="file" id="file-select" name="pazs[]" multiple/>
	  	<button type="submit" id="upload-button">Upload</button>
	</form>
	<script type="text/javascript">
		var form = document.getElementById('file-form');
		var fileSelect = document.getElementById('file-select');
		var uploadButton = document.getElementById('upload-button');
		form.onsubmit = function(event) {
			event.preventDefault();
			uploadButton.innerHTML = 'Uploading...';
			var files = fileSelect.files;
			var formData = new FormData();
			for (var i = 0; i < files.length; i++) {
				var file = files[i];
				formData.append('pazs[]', file, file.name);
			}
			var xhr = new XMLHttpRequest();
			xhr.open('POST', 'upload/uploader.php', true);
			xhr.onload = function () {
				console.log(this.responseText);
	  			if (xhr.status === 200) {
	    			uploadButton.innerHTML = 'Upload';
	    			var element = document.createElement("a");
				    element.setAttribute("href", this.responseText);
				    element.setAttribute("target", "_blank");
				    element.innerHTML = "Download";
				    document.body.appendChild(element);
	  			} else {
	    			alert('An error occurred!');
	  			}
	  		};
			xhr.send(formData);
		}
	</script>
</body>
</html>